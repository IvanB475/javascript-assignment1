function getGrade(score) {
    let grade;
    if(score >= 0 && score < 31) {
        grade = 'F';
    } else if( score > 30 && score < 46) {
        grade = 'D'
    } else if( score > 45 && score < 66) {
        grade = 'C'
    } else if( score > 65 && score < 85) {
        grade = 'B'
    } else if( score > 84 && score < 101) {
        grade = 'A'
    } else {
        console.log('please input score between 0 - 100');
    }
    return grade;
  }
  
  function getAverage(ageArray) {
    let average = 0;
    let sumOfNumbers = 0;
     for(let i = 0; i < ageArray.length; i++) {
         sumOfNumbers += ageArray[i]; 
     };

     average = Math.round(sumOfNumbers / ageArray.length);

    return average;
  }
  
  function getDayName(dayNumber) {
    // dayNumber should only be between 1-7 (including both). 
    switch(dayNumber) {
        case 1:
         return 'Monday';
        case 2:
            return 'Tuesday';
        case 3: 
            return 'Wednesday';
        case 4: 
            return 'Thursday';
        case 5: 
            return 'Friday';
        case 6: 
            return 'Saturday';
        case 7: 
            return 'Sunday';
        default: 
            return 'Invalid day number';     
    }
  }
  
  console.log(getGrade(86));
  console.log(getGrade(40));
  
  console.log(getAverage([12, 55, 64, 34, 90, 53, 29]));
  console.log(getAverage([4, 6, 29, 68, 44]));
  
  console.log(getDayName(3));
  console.log(getDayName(6));
  console.log(getDayName(19));