// require necessary modules
const express = require('express');
require('dotenv').config();
const app = express();
const helmet = require('helmet');

// require necessary files
const weather = require('./weather/weather');
const geometry = require('./geometry/geometry');
const weatherAPI = require('./weather/weatherAPI');
const geometryAPI = require('./geometry/geometryAPI');

//get env variables
const PORT = process.env.PORT  || 3000;

// set up basic middlewares
app.use(express.json());
app.disable('x-powered-by');
app.use(helmet());


app.use(weatherAPI);
app.use(geometryAPI);


//start the server
app.listen(PORT, () => {
    console.log("Server is listening on port: " + PORT);
})
