// create class shape with 2 parameters and 2 methods
class Shape {
    constructor(x,y) {
        this.x = parseFloat(x);
        this.y = parseFloat(y);
    }

    perimeter() {
        return undefined;
    }

    area() {
        return undefined;
    }
}

// create class rectangle that extends shape and overrides it's methods
class Rectangle extends Shape {
    perimeter() {
        try {
            const perimeter = 2 * (this.x + this.y);
            return perimeter;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    };

    area() {
        try { 
            const area = this.x * this.y;
            return area;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    }
}

// create class square that extends shape and overrides it's methods
class Square extends Shape {
    perimeter() {
        try { 
            const perimeter = 4 * this.x;
            return perimeter;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    }

    area() {
        try { 
            const area = this.x * this.x;
            return area;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    }
}


// create class circle that extends shape and overrides it's methods
class Circle extends Shape {
    perimeter() {
        try { 
            const perimeter = Math.PI * this.x;
            return perimeter;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    }

    area() {
        try { 
            const r = this.x / 2;
            const area = Math.PI * r;
            return area;
        } catch(err) {
            console.log(err.message);
            return err;
        }
    }
}


// initialize new instance of Rectangle class, calculate perimeter and area and print to console
const rectangle = new Rectangle(5,10);
const rectanglePerimeter = rectangle.perimeter();
const rectangleArea = rectangle.area();
console.log('The perimeter of rectangle is: ' + rectanglePerimeter);
console.log('The area of rectangle is: ' + rectangleArea);

// initialize new instance of Square class, calculate perimeter and area and print to console
const square = new Square(5, undefined);
const squarePerimeter = square.perimeter();
const squareArea = square.area();
console.log('The perimeter of square is: ' + squarePerimeter);
console.log('The area of square is: ' + squareArea);


// initialize new instance of Circle class, calculate perimeter and area and print to console
const circle = new Circle(10, undefined);
const circlePerimeter = circle.perimeter();
const circleArea = circle.area();
console.log('The perimeter of circle is: ' + circlePerimeter);
console.log('The area of circle is: ' + circleArea);


// export all classes
module.exports = {Shape, Rectangle, Square, Circle};