// require necessary classes
const { Rectangle, Circle, Square } = require('./geometry');

// function that takes in 3 query params, initializes new instance of rectangle, and returns requested result 
exports.getRectangleInfo = async (req, res, next) => {
    try { 
        const { x, y, get} = req.query;
        const rectangle = new Rectangle(x,y);
        const result = get == 'perimeter' ? rectangle.perimeter() : rectangle.area();
        res.status(200).json(result);
    } catch(err) {
        next(err);
    }
}

// function that takes in 3 query params, initializes new instance of circle, and returns requested result 
exports.getCircleInfo = async (req, res, next) => {
    try { 
        const { x, y, get} = req.query;
        const circle = new Circle(x,y);
        const result = get == 'perimeter' ? circle.perimeter() : circle.area();
        res.status(200).json(result);
    } catch(err) {
        next(err);
    }
}

// function that takes in 3 query params, initializes new instance of square, and returns requested result 
exports.getSquareInfo = async (req, res, next) => {
    try { 
        const { x, y, get} = req.query;
        const square = new Square(x,y);
        const result = get == 'perimeter' ? square.perimeter() : square.area();
        res.status(200).json(result);
    } catch(err) {
        next(err);
    }
}