// require express and define router 
const express = require('express');
const router = express.Router();


// require controllers file
const geometryControllers = require('./geometryControllers');


// registered api route to get /rectangleInfo, api takes in 3 query params: x, y, get
router.get('/rectangleInfo', geometryControllers.getRectangleInfo);

// registered api route to get /squareInfo, api takes in 3 query params: x, y, get
router.get('/squareInfo', geometryControllers.getSquareInfo);

// registered api route to get /circleInfo, api takes in 3 query params: x, y, get
router.get('/circleInfo', geometryControllers.getCircleInfo);



module.exports = router;