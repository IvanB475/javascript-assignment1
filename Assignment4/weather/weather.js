// require necessary modules
const axios = require('axios');

// get api key from env variable
const api_key = process.env.WEATHER_API;


// create a class with constructor that takes one parameter city and has one method getForecast
class WeatherInfo {
    constructor(city) {
        this.city = city;
    }

    async getForecast() {
        try { 
        const forecast = await axios.get('https://api.weatherapi.com/v1/forecast.json', { params: { key: api_key, q: this.city} });
        return forecast.data;
        } catch(err) {
            return err;
        }
    }
}

//Initialize two instances of the class WeatherInfo
const Paris = new WeatherInfo('Paris');
const London = new WeatherInfo('London');

//Simple function to handle logging results to console
setTimeout(async () => {
    console.log(await Paris.getForecast());
    console.log(await London.getForecast());
}, 1000)

//Export WeatherInfo class
module.exports = WeatherInfo;
