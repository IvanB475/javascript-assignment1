const express = require('express');
const router = express.Router();
const weatherController = require('./weatherController');

// register route to /forecast
router.get('/forecast', weatherController.getWeatherInfo);


//export router
module.exports = router;