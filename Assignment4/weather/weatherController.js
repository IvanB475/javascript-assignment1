// Import  WeatherInfo class
const Weather = require('./weather');

// function that handles incoming request, expects to get city as query param and responds with forecast data
exports.getWeatherInfo = async (req, res, next) => {
    try { 
    const { city } = req.query;
    const weatherClass = new Weather(city);
    const forecast = await weatherClass.getForecast();
    res.status(200).json(forecast);
    } catch(err) {
        console.log(err.message);
        next(err);
    }
}