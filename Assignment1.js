

// 2 strings - first  immutable and 2nd mutable
const stringOne = "first string";
let stringTwo = "";

// 2 numbers - first  immutable and 2nd mutable
const numberOne = 1;
let numberTwo = 2.3;

// 2 booleans - first  immutable and 2nd mutable
const amITrue = true;
let amIFalse = false;


//null - mutable
let youCanChangeMe = null;

//undefined - immutable
const youCantChangeMe = undefined;

//print each variable in it's own line with a number
console.log(`1 - ${stringOne}`);
console.log(`2 - ${stringTwo}`);
console.log(`3 - ${numberOne}`);
console.log(`4 - ${numberTwo}`);
console.log(`5 - ${amITrue}`);
console.log(`6 - ${amIFalse}`);
console.log(`7 - ${youCanChangeMe}`);
console.log(`8 - ${youCantChangeMe}`);

// print type of variable in it's own line
console.log(typeof(stringOne));
console.log(typeof(stringTwo));
console.log(typeof(numberOne));
console.log(typeof(numberTwo));
console.log(typeof(amITrue));
console.log(typeof(amIFalse));
console.log(typeof(youCanChangeMe));
console.log(typeof(youCantChangeMe));


// declaring 4 immutable variables
const resultOfSum = numberOne + numberTwo;
const resultOfSubstraction = numberOne - numberTwo;
const resultOfMultiplication = numberOne * numberTwo;
const resultOfDivision = numberOne / numberTwo;

// printing newly declared variables line by line
console.log(resultOfSum);
console.log(resultOfSubstraction);
console.log(resultOfMultiplication);
console.log(resultOfDivision);


//assinging text to empty variable
stringTwo = "Galatasaray is my favourite club";

//printing both string variables using string literals
console.log(stringOne + ' ' + stringTwo);

// converting number variables to strings and assinging to new variables
const numberOneToString = numberOne + " ";
const numberTwoToString = numberTwo + " ";

// printing results of typecasting
console.log(numberOneToString);
console.log(numberTwoToString);

//compare null and undefined variable using normal check
const normalComparison = youCanChangeMe == youCantChangeMe;

//compare null and undefined variable using strict check
const strictComparison = youCanChangeMe === youCantChangeMe;

// printing results of comparisons
console.log('normal comparison: ' + normalComparison);
console.log('strict comparison: ' + strictComparison);