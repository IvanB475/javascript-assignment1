

// implented function factorial that takes in one parameter, checks if it's number & integer and returns result or throws an error
function factorial(oneNumber) {
    try { 
        if(typeof((oneNumber)) == 'number' && Number.isInteger(oneNumber)) {
            return oneNumber;
        } else {
            throw Error ('Input an integer');
        }
    }
    catch(error){
        console.log(error.message);
    }
}

// initializing new object and assinging it to variable userInfo
const userInfo = {
    name: 'Ivan',
    gender: 'Male',
    hasAPet: false,
    numberOfCountriesVisited: 6
}

// creating a clone of userInfo object
const cloneOne = {...userInfo};

//creating clone of cloneOne object
const cloneNamedDifferent = { ...cloneOne, name: 'new name'};

// comparing userInfo and cloneOne objects
const areTheyEqual = userInfo === cloneOne;
console.log(`Are the objects equal? ${areTheyEqual} --> Because by creating clone, objects refer to different place in memory`);


// initializing new object with add
const address = { address: 'this is my address'};

// adding address field to userInfo and cloneNamedDifferent object and assigning them address variable 
userInfo.address = address;
cloneNamedDifferent.address = address;

// comparing address fields of two objects
const areAddressFieldsEqual = userInfo.address === cloneNamedDifferent.address;
console.log(`Are the address fields in objects equal? ${areAddressFieldsEqual} --> Because they point to same place in memory`);

// create copy of each object in array, add it to new array and delete wanted field from each object
const deleteField = (arrayOfObjects, fieldToDelete) => {
    const newArray = [];
    arrayOfObjects.forEach((obj) => {
        newArray.push({...obj});
    });
    newArray.forEach((obj) => {
        delete obj[fieldToDelete];
    })
    return newArray;
}


// create constructor function that takes 2 parameters and returns object with 4 properties
function Rectangle(a, b) {
    this.length = a;
    this.width = b;
    this.perimeter = 2 * (a + b);
    this.area = a * b;

    const rectangle = {
        length: this.length,
        width: this.width,
        perimeter: this.perimeter,
        area: this.area
    }
    if(new.target) {
    return rectangle;
    } else {
        console.log("you didn't call with new keyword");
    }
}

// create a function that takes in one parameter and returns an object containing max and min fields
const getMaxAndMin = (numbers) => {
    const maxAndMinObj = {
        max: 0,
        min: 0
    }
    numbers.forEach((number, i) => {
        if(number < 0 || number > 100) {
            console.log("input proper number");
        } else {
            if((i === 0) || (number < maxAndMinObj.min)) {
                maxAndMinObj.min = number;
            } else if(number > maxAndMinObj.max) {
                maxAndMinObj.max = number;
            }
        }
    })
    return maxAndMinObj;
}